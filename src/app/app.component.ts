import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'calc';
  input: string = '';

  result: string = '';

  display(num: string) {
    this.input = this.input + num;
    console.log(this.input);
  }
  reset() {
    this.result = '';
    this.input = '';
  }
  off() {
    this.result = '';
    this.input = '';
  }
  calcAnswer() {
    let formula = this.input;
    this.result = eval(formula);
  }
  getAnswer() {
    this.calcAnswer();
    this.input = this.result;
    if (this.input == '0') this.input = '0';
  }
  delete() {
    if (this.input != '') {
      this.input = this.input.substring(0, this.input.length - 1);
    }
  }
}
